TARGET= projet
CFLAGS=-g -W -Wall -Wextra -O2
SRC=$(wildcard src/*.c)
OBJ=$(subst src/,obj/,$(subst .c,.o,$(SRC)))
DIR= obj

default: $(TARGET)



$(shell mkdir -p $(DIR))

obj/%.o: src/%.c
	gcc $(CFLAGS) -c $< -o $@

$(TARGET): $(OBJ)
	gcc $^ -o $@



.PHONY: clean
clean:
	rm -f $(OBJ)
	rm -f $(TARGET)
	rmdir $(DIR)