#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE_NAME 100
#define SIZE_IATA_AL 3
#define SIZE_IATA_AP 4

#define INT_NULL \
    0xCAFEBABE // valeur affectée aux champs de type int quand le champ du .csv est vide

#define TABLE_SIZE_AP 702 // hash_func("ZZZ")
#define TABLE_SIZE_AL 100
#define TABLE_SIZE_DATE 380

#define MAX_ESCALES 3
#define MIN_TIME_ESCALE 30 // 30 minutes
#define NB_ITI_DEFAULT 3   // nombre d'itinéraires affichés par défaut

//------------SD GLOBALES---------------//

// SD POUR AEROPORTS
typedef struct ap
{
    char IATA_port[SIZE_IATA_AP];
    char name[SIZE_NAME];
    char ville[SIZE_NAME];
    char state[SIZE_IATA_AL];
} Ap;

typedef struct cellule_ap
{ // liste chainée d'aeroport
    Ap *               a;
    struct cellule_ap *suiv;
} Cell_ap;
typedef Cell_ap *Liste_ap;


// SD POUR AIRLINES
typedef struct al
{ // structure initiale compagnie
    char IATA_line[SIZE_IATA_AL];
    char name[SIZE_NAME];
} Al;

typedef struct cellule_al
{ // liste chainée d'airlines
    Al *               a;
    struct cellule_al *suiv;
} Cell_al;
typedef Cell_al *Liste_al;


// SD POUR FLIGHTS
typedef struct flight
{ // structure initiale
    short int month;
    short int day;
    short int weekday;
    char      airline[SIZE_IATA_AL];
    char      org_ap[SIZE_IATA_AP];
    char      dest_ap[SIZE_IATA_AP];
    int       sched_dep;
    float     dep_delay;
    float     air_time;
    int       dist;
    int       sched_arr;
    float     arr_delay;
    short int diverted;
    short int cancelled;
} Fl;

typedef struct cellule_flight
{ // liste chainée de vols
    Fl *                   fl;
    struct cellule_flight *suiv;
} Cell_fl;
typedef Cell_fl *Liste_fl;

//---------SD SPECIFIQUES A CERTAINES REQUETES----------------//

// Cellule pour table de hash des vols triés par org_ap (cf rapport)
typedef struct cellule_hash
{
    char                 key[SIZE_IATA_AP];
    Liste_fl             l;
    struct cellule_hash *suiv;
} Cell_hash;
typedef Cell_hash *Liste_cell_hash;


// Cellule pour stocker des IATA (airports ou airlines) pour requetes : show-airports/show-airlines
typedef struct cell_s
{
    char           s[SIZE_IATA_AP];
    struct cell_s *suiv;
} Cell_s;
typedef Cell_s *Liste_s;


// Cellule qui stock le délai moyen de chaque airline
typedef struct cellule_delay_al
{
    char                     IATA[SIZE_IATA_AL];
    float                    avg_delay;
    int                      nb_fl;
    struct cellule_delay_al *suiv;
} Cell_del_al;
typedef Cell_del_al *Liste_del_al;