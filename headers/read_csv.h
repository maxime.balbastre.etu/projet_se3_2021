
#include "requetes.h"

// Fonctions générales
void  erase_end_string (char *line);
char *strtok_r_m (char *s, const char *delim, char **save_ptr);
char *strtok_m (char *s, const char *delim);


// Fonctions de lectures des fichiers csv, remplissage des SD
int read_ap (FILE *fairports, Liste_ap table[]);
int read_al (FILE *fairlines, Liste_al table[]);
int read_fl (FILE *          fflights,
             Liste_fl        tab_line[],
             Liste_fl        tab_date[],
             Liste_cell_hash tab_org_ap[],
             Liste_cell_hash tab_dest_ap[],
             Liste_del_al    tab_del_al[],
             Liste_fl *      delayed_fl);

// Libération totale de la mémoire
void desalloc_all_memory (Liste_al        tab_al[],
                          Liste_ap        tab_ap[],
                          Liste_fl        tab_line[],
                          Liste_fl        tab_date[],
                          Liste_cell_hash tab_org_ap[],
                          Liste_cell_hash tab_dest_ap[],
                          Liste_del_al    tab_del_al[],
                          Liste_fl *      delayed_fl);
