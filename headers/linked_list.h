

#include "structures.h"


// Affichage structure de bases
void print_ap (Ap *a);
void print_al (Al *a);
void print_fl (Fl *f);

// Affichage listes chaînées
void print_list_ap (Liste_ap);
void print_list_al (Liste_al);
void print_list_fl (Liste_fl);


// Ajouts listes chaînées
void ajout_tete_s (Liste_s *pl, char *s);
void ajout_tete_ap (Liste_ap *pliste_ap, Ap **ap);
void ajout_tete_al (Liste_al *pliste_al, Al **al);
void ajout_tete_fl (Liste_fl *pliste_fl, Fl **fl);

void ajout_dec_fl (Liste_fl *pl, Fl **new);

// Libération de la mémoire allouée
void desalloc_struct_fl (Liste_fl t[]);
void supp_queue_fl (Liste_fl *pl);
void supp_tete_fl (Liste_fl *pl);
void desalloc_liste_fl (Liste_fl *pl);
void supp_tete_al (Liste_al *pl);
void desalloc_liste_al (Liste_al *pl);
void supp_tete_ap (Liste_ap *pl);
void desalloc_liste_ap (Liste_ap *pl);
void desalloc_liste_s (Liste_s *pl);
void supp_tete_del_al (Liste_del_al *pl);
void desalloc_liste_del_ap (Liste_del_al *pl);
void supp_tete_cell_hash (Liste_cell_hash *pl);
void desalloc_liste_cell_hash (Liste_cell_hash *pl);