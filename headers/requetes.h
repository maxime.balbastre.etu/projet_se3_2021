#include "hash_table.h"

void delayed_airline (Liste_del_al t[], Liste_al t_al[], char *asked_al);

// Most Delayed Airlines

void ajout_tete_delay_al (Liste_del_al *pl, Cell_del_al *a);
void supp_queue_delay_al (Liste_del_al *pl);
void ajout_dec_delay_al (Liste_del_al *pl, Cell_del_al *pc);
void print_liste_delay_al (Cell_del_al *l, Liste_al t_al[]);
void most_delayed_airlines (Liste_del_al t[], Liste_al t_al[], int max);

void changed_flights (Liste_fl tab[], int m, int d);

void show_flights (Liste_fl t[], char *IATA_ap, int m, int d, int time, int max);

// void ajout_most_delayed_flights(Liste_fl* pl, Fl** new, int nb_fl);

// Show airports
void print_list_s (Liste_s l);
int  ajout_ordo_s (Liste_s *pl, char s[], int nb);
void show_airports (Liste_fl t[], Liste_ap t_ap[], char *al, int nb_ap);

void avg_flight_duration (Liste_cell_hash t[], char *dep, char *arr);

void show_airlines (Liste_cell_hash t[], Liste_al t_al[], char *ap, int nb_ap);

void most_delayed_airlines_at_airports (Liste_cell_hash t_ap[], Liste_al t_al[], char *ap, int max);

void find_flight (Liste_fl  l,
                  Liste_fl *iti,
                  char *    dep,
                  char *    iata1,
                  char *    dest,
                  int       time,
                  int       max_iti,
                  int       nb_escales,
                  int *     pt_nb_iti);

void find_itinerary (Liste_fl t_date[], int m, int d, char *dep, char *dest, int time, int max_iti);

void find_multicity_itinerary(Liste_fl t_date[], char *liste_args[]);

void ajout_most_delayed_flights (Liste_fl *pl, Fl **new, int nb_fl);
