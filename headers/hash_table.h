#include "linked_list.h"


// Fonctions tables de hachages
void init_hash_table_ap (Liste_ap t[]);
void init_hash_table_al (Liste_al t[]);
void init_hash_table_fl (Liste_fl t[], int table_size);
void init_hash_table_fl_ap (Liste_cell_hash t[]);
void init_hash_table_del_al (Liste_del_al t[]);

void ajout_hash_table_ap (Liste_ap t[], Ap **ap);
void ajout_hash_table_al (Liste_al t[], Al **al);
void ajout_hash_table_fl (Liste_fl t[], Fl **fl, int hash_func (char *), char *key);
void ajout_hash_table_fl_ap (Liste_cell_hash t[], Fl **fl);
void ajout_hash_table_fl_date (Liste_fl t[], Fl **fl);

void create_cell_del_al (Liste_del_al *l, char al[], float delay);
void incr_hash_table_delay_al (Liste_del_al t[], char al[], float delay);

// Fonctions de hachages
int hash_function (char *word);
int hash_func_IATA_p (char *word);
int hash_func_date (int month, int day);

// Libération de la mémoire
void desalloc_table_hash_fl (Liste_fl t[], int t_size);
void desalloc_table_hash_al (Liste_al t[], int t_size);
void desalloc_table_hash_ap (Liste_ap t[], int t_size);
void desalloc_table_hash_del_al (Liste_del_al t[], int t_size);
void desalloc_table_hash_fl_ap (Liste_cell_hash t[], int t_size);

// Affichages tables de hachage
void print_hash_table_ap (Liste_ap t[]);
void print_hash_table_al (Liste_al t[]);
void print_hash_table_fl (Liste_fl t[], int table_size);
void print_hash_list (Liste_cell_hash t[]);
