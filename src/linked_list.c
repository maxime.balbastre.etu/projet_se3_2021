
#include "../headers/linked_list.h"


// Affichage structures de base
void print_ap (Ap *a)
{
    printf ("%s %s %s %s\n", a->IATA_port, a->name, a->ville, a->state);
}

void print_al (Al *a) { printf ("%s %s\n", a->IATA_line, a->name); }

void print_fl (Fl *f)
{
    printf ("%d,%d,%d,", f->month, f->day, f->weekday);
    printf ("%s,%s,%s,", f->airline, f->org_ap, f->dest_ap);
    if ((unsigned)f->sched_dep == INT_NULL)
        printf (",");
    else
        printf ("%d,", f->sched_dep);

    if (f->dep_delay == (float)INT_NULL)
        printf (",");
    else
        printf ("%.2f,", f->dep_delay);

    if (f->air_time == (float)INT_NULL)
        printf (",");
    else
        printf ("%.2f,", f->air_time);

    if ((unsigned)f->dist == INT_NULL)
        printf (",");
    else
        printf ("%d,", f->dist);

    if ((unsigned)f->sched_arr == INT_NULL)
        printf (",");
    else
        printf ("%d,", f->sched_arr);

    if (f->arr_delay == (float)INT_NULL)
        printf (",");
    else
        printf ("%.2f,", f->arr_delay);
    printf ("%d,%d\n", f->diverted, f->cancelled);

}

// Affichage listes chaînées

void print_list_ap (Liste_ap l)
{
    if (l == NULL) printf ("vide");
    while (l != NULL)
    {
        printf ("%s", l->a->IATA_port);
        l = l->suiv;
    }
    printf ("\n");
}

void print_list_al (Liste_al l)
{
    if (l == NULL) printf ("vide");
    while (l != NULL)
    {
        printf ("%s", l->a->IATA_line);
        l = l->suiv;
    }
    printf ("\n");
}

void print_list_fl (Liste_fl pf)
{
    if (pf == NULL) printf ("vide");
    while (pf != NULL)
    {
        print_fl (pf->fl);
        pf = pf->suiv;
    }
}


// Ajouts listes chaînées
void ajout_tete_ap (Liste_ap *pliste_ap, Ap **ap)
{
    Cell_ap *c = malloc (sizeof (Cell_ap));
    c->a       = *ap;
    c->suiv    = *pliste_ap;
    *pliste_ap = c;
}

void ajout_tete_al (Liste_al *pliste_al, Al **al)
{
    Cell_al *c = malloc (sizeof (Cell_al));
    c->a       = *al;
    c->suiv    = *pliste_al;
    *pliste_al = c;
}

void ajout_tete_fl (Liste_fl *pliste_fl, Fl **fl)
{
    Cell_fl *c = malloc (sizeof (Cell_fl));
    c->fl      = *fl;
    c->suiv    = *pliste_fl;
    *pliste_fl = c;
}

void ajout_tete_s (Liste_s *pl, char *s)
{
    Cell_s *new = malloc (sizeof (Cell_s));
    strcpy (new->s, s);
    new->suiv = *pl;
    *pl       = new;
}

void ajout_dec_fl (Liste_fl *pl, Fl **new)
{ // ajout ordonnée décroissant
    if ((*pl) == NULL)
    {                            // si le vol de la nvlle cell a un retard
        ajout_tete_fl (pl, new); // plus grand on ajoute en tete
        return;
    }
    if ((*new)->arr_delay > (*pl)->fl->arr_delay)
    {
        ajout_tete_fl (pl, new);
        return;
    }
    Cell_fl *p = *pl;
    while (p->suiv != NULL && (*new)->arr_delay <= p->suiv->fl->arr_delay)
        p = p->suiv;
    ajout_tete_fl (&p->suiv, new);
}


// Suppresions
void desalloc_struct_fl (Liste_fl t[])
{ // libère la mémoire allouée pour les structures fl (uniques) vers lesquelles toutes les SD pointent
    for (int i = 0; i < TABLE_SIZE_AL; ++i)
    {
        Cell_fl *p = t[i];
        while (p != NULL)
        {
            free (p->fl);
            p = p->suiv;
        }
    }
}

void supp_queue_fl (Liste_fl *pl)
{
    if (*pl == NULL) return;
    Cell_fl *p = *pl;
    while (p->suiv->suiv != NULL) p = p->suiv;
    Cell_fl *temp = p->suiv;
    p->suiv       = NULL;
    free (temp);
}


void supp_tete_fl (Liste_fl *pl)
{ // supprime le premier élement de la liste + désalloue la case mémoire du vol (unique)
    if (*pl == NULL) return;
    Cell_fl *pTmp = *pl;
    *pl           = (*pl)->suiv;
    free (pTmp);
}


void desalloc_liste_fl (Liste_fl *pl)
{
    if (*pl == NULL) return;
    while ((*pl != NULL))
    {
        supp_tete_fl (pl);
    }
}

void supp_tete_al (Liste_al *pl)
{
    if (*pl == NULL) return;
    free ((*pl)->a);
    Cell_al *pTmp = *pl;
    *pl           = (*pl)->suiv;
    free (pTmp);
}

void desalloc_liste_al (Liste_al *pl)
{
    if (*pl == NULL) return;
    while ((*pl != NULL))
    {
        supp_tete_al (pl);
    }
}

void supp_tete_ap (Liste_ap *pl)
{
    if (*pl == NULL) return;
    free ((*pl)->a);
    Cell_ap *pTmp = *pl;
    *pl           = (*pl)->suiv;
    free (pTmp);
}

void desalloc_liste_ap (Liste_ap *pl)
{
    if (*pl == NULL) return;
    while ((*pl != NULL))
    {
        supp_tete_ap (pl);
    }
}

void supp_tete_s (Liste_s *pl)
{
    if (*pl == NULL) return;
    Cell_s *pTmp = *pl;
    *pl          = (*pl)->suiv;
    free (pTmp);
}

void desalloc_liste_s (Liste_s *pl)
{
    if (*pl == NULL) return;
    while ((*pl != NULL))
    {
        supp_tete_s (pl);
    }
}

void supp_tete_del_al (Liste_del_al *pl)
{
    if (*pl == NULL) return;
    Cell_del_al *pTmp = *pl;
    *pl               = (*pl)->suiv;
    free (pTmp);
}


void desalloc_liste_del_ap (Liste_del_al *pl)
{
    if (*pl == NULL) return;
    while ((*pl != NULL))
    {
        supp_tete_del_al (pl);
    }
}

void supp_tete_cell_hash (Liste_cell_hash *pl)
{
    if (*pl == NULL) return;
    desalloc_liste_fl (&(*pl)->l);
    Cell_hash *pTmp = *pl;
    *pl             = (*pl)->suiv;
    free (pTmp);
}


void desalloc_liste_cell_hash (Liste_cell_hash *pl)
{
    if (*pl == NULL) return;
    while ((*pl != NULL))
    {
        supp_tete_cell_hash (pl);
    }
}
