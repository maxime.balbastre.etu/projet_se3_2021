#include "../headers/linked_list.h"


// Fonctions de hachages
int hash_function (char *word)
{
    int cpt = 0;
    int ind = 0;
    while (word[cpt] != '\0')
    {
        if (word[cpt] > 64) // si c'est une lettre
            ind += (word[cpt] - 64);
        else if (word[cpt] > 47 && word[cpt] < 58)
        { // si c'est un chiffre
            ind += word[cpt] - 48;
        }
        cpt++;
    }
    return ind;
}

int hash_func_IATA_p (char *word)
{
    int ind = 0;
    ind += word[0] - 64;
    ind *= word[1] - 64;
    ind += word[2] - 64;
    return ind;
}

int hash_func_date (int month, int day)
{ // la date est sous la forme mmjj,
    int ind = (month - 1) * 31 + day;
    return ind;
}

// Fonctions tables hachage
void init_hash_table_ap (Liste_ap t[])
{
    for (int i = 0; i < TABLE_SIZE_AP; i++)
    {
        t[i] = NULL;
    }
}

void init_hash_table_al (Liste_al t[])
{
    for (int i = 0; i < TABLE_SIZE_AL; i++)
    {
        t[i] = NULL;
    }
}

void init_hash_table_fl (Liste_fl t[], int table_size)
{
    for (int i = 0; i < table_size; i++)
    {
        t[i] = NULL;
    }
}

void init_hash_table_fl_ap (Liste_cell_hash t[]) // table de hachage des vols triés par ap
{ 
    for (int i = 0; i < TABLE_SIZE_AP; i++)
    {
        t[i] = NULL;
    }
}

void init_hash_table_del_al (Liste_del_al t[])
{
    for (int i = 0; i < TABLE_SIZE_AL; i++) t[i] = NULL;
}

void ajout_hash_table_ap (Liste_ap t[], Ap **ap)
{
    int ind = hash_func_IATA_p ((*ap)->IATA_port);
    ajout_tete_ap (&t[ind], ap);
}

void ajout_hash_table_al (Liste_al t[], Al **al)
{
    int ind = hash_function ((*al)->IATA_line);
    ajout_tete_al (&t[ind], al);
}

void ajout_hash_table_fl (Liste_fl t[], Fl **fl, int hash_func (char *), char *key)
{
    int ind = hash_func (key);
    ajout_tete_fl (&t[ind], fl);
}

void ajout_hash_table_fl_ap (Liste_cell_hash t[], Fl **fl)
{ // ajout dans hashtable de vols triés par ap
    int ind = hash_func_IATA_p ((*fl)->org_ap);
    if (t[ind] == NULL)
    {
        Cell_hash *new = malloc (sizeof (Cell_hash));
        strcpy (new->key, (*fl)->org_ap);
        new->l    = NULL;
        new->suiv = NULL;
        t[ind]    = new;
        ajout_tete_fl (&(t[ind]->l), fl);
        return;
    }
    Cell_hash *p = t[ind];
    while (p->suiv != NULL && (strcmp (p->key, (*fl)->org_ap) != 0))
    {
        p = p->suiv;
    }
    if (p->suiv == NULL && (strcmp (p->key, (*fl)->org_ap) != 0)) // cas ou la cellule correspondant a l'aeroport de fl n'est pas encore créée
    { 
        Cell_hash *new = malloc (sizeof (Cell_hash));
        strcpy (new->key, (*fl)->org_ap);
        new->l    = NULL;
        new->suiv = NULL;
        p->suiv   = NULL;
        ajout_tete_fl (&(p->suiv->l), fl);
        return;
    }
    ajout_tete_fl (&(p->l), fl); // cas ou la cellule pour l'org_ap de fl est déjà créée, on ajoute simplement
}


void ajout_hash_table_fl_date (Liste_fl t[], Fl **fl)
{
    int ind = hash_func_date ((*fl)->month, (*fl)->day);
    ajout_tete_fl (&t[ind], fl);
}

// Les deux fonction suivantes sont pour une SD spécifique aux deux requêtes : delayed-airlines et most-delayed-airlines
void create_cell_del_al (Liste_del_al *l, char al[], float delay)
{
    Cell_del_al *new = malloc (sizeof (Cell_del_al));
    strcpy (new->IATA, al);
    if (delay != (float)INT_NULL && delay > 0)
        new->avg_delay = delay;
    else
        new->avg_delay = 0;
    new->nb_fl = 1;
    new->suiv  = *l;
    *l = new; // raccordement de la nouvelle cellule à la table de hachage
}

void incr_hash_table_delay_al (Liste_del_al t[], char al[], float delay)
{ // incrémente le délai de la airline correspondante
    int ind = hash_function (al);
    if (t[ind] == NULL)
    { // cas où c'est le premier vol de la compagnie que l'on traite
        create_cell_del_al (&t[ind], al, delay);
        return;
    }
    Cell_del_al *p = t[ind];
    while (p != NULL && strcmp (p->IATA, al) != 0) // gestion collisions
        p = p->suiv;
    if (p == NULL)
    { // cas où il y a une ou des collisions mais que c'est qd meme le premier vol de cette compagnie que l'on traite
        create_cell_del_al (&t[ind], al, delay);
        return;
    }
    if (delay != (float)INT_NULL && delay > 0)
    { // on n'incrémente pas la somme des délais si le delai de ce vol est négatif (c'est a dire en avance)
        p->nb_fl++;
        p->avg_delay = (p->avg_delay + delay);
    }
}


// Libération de la mémoire
void desalloc_table_hash_fl (Liste_fl t[], int t_size)
{
    for (int i = 0; i < t_size; ++i) desalloc_liste_fl (&t[i]);
}

void desalloc_table_hash_fl_ap (Liste_cell_hash t[], int t_size)
{
    for (int i = 0; i < t_size; ++i) desalloc_liste_cell_hash (&t[i]);
}

void desalloc_table_hash_al (Liste_al t[], int t_size)
{
    for (int i = 0; i < t_size; ++i) desalloc_liste_al (&t[i]);
}

void desalloc_table_hash_ap (Liste_ap t[], int t_size)
{
    for (int i = 0; i < t_size; ++i) desalloc_liste_ap (&t[i]);
}

void desalloc_table_hash_del_al (Liste_del_al t[], int t_size)
{
    for (int i = 0; i < t_size; ++i) desalloc_liste_del_ap (&t[i]);
}


// Affichages tables hachage
void print_hash_table_ap (Liste_ap t[])
{
    for (int i = 0; i < TABLE_SIZE_AP; i++)
    {
        printf ("t[%d]: ", i);
        print_list_ap (t[i]);
    }
}

void print_hash_table_al (Liste_al t[])
{
    for (int i = 0; i < TABLE_SIZE_AL; i++)
    {
        print_list_al (t[i]);
    }
}

void print_hash_table_fl (Liste_fl t[], int table_size)
{
    for (int i = 0; i < table_size; i++)
    {
        print_list_fl (t[i]);
    }
}

void print_hash_list (Liste_cell_hash t[])
{
    int nb = 0;
    for (int i = 0; i < TABLE_SIZE_AP; ++i)
    {
        Cell_hash *p = t[i];
        if (p == NULL) printf ("vide\n");
        while (p != NULL)
        {
            printf ("Ap: %s\n", p->key);
            print_list_fl (p->l);
            nb++;
            printf ("\n");
            p = p->suiv;
        }
    }
    printf ("%d\n", nb);
}