#include "../headers/read_csv.h"

#define LINE_SIZE 100     // taille max d'une ligne correspondant à une requête
#define MAX_REQUETES 100  // nbr max de requetes dans le fichier
#define MAX_PARAMETRES 20 // nbr max de parametres dans une requete

int main ()
{
    // OUVERTURE DES FICHIERS .CSV
    FILE *fairlines, *fairports, *fflights;
    fairlines = fopen ("data/airlines.csv", "r");
    fairports = fopen ("data/airports.csv", "r");
    fflights  = fopen ("data/flights.csv", "r");

    if (!fairlines || !fairports || !fflights)
    {
        printf ("Erreur lors du chargement des fichiers.\n");
        exit (EXIT_FAILURE);
    }

    // RECUPÉRATIONS DONNÉES CSV

    // Lecture de airports.csv
    Liste_ap ht_ap[TABLE_SIZE_AP];
    init_hash_table_ap (ht_ap);
    int nb_ap = read_ap (fairports, ht_ap);
    // print_hash_table_ap(ht_ap);

    // Lecture de airlines.csv
    Liste_al ht_al[TABLE_SIZE_AL];
    init_hash_table_al (ht_al);
    int nb_al = read_al (fairlines, ht_al);
    // print_hash_table_al(ht_al);

    // Lecture de flights.csv
    Liste_fl delayed_fl = NULL; // Liste chaînée des 5 vols les plus retardés

    Liste_fl ht_fl_al[TABLE_SIZE_AL]; // Hash table des vols triés par airline
    init_hash_table_fl (ht_fl_al, TABLE_SIZE_AL);

    Liste_fl ht_fl_date[TABLE_SIZE_DATE]; // Hash table des vols triés par date de vol
    init_hash_table_fl (ht_fl_date, TABLE_SIZE_DATE);

    Liste_cell_hash ht_fl_org_ap[TABLE_SIZE_AP]; // Hash table des vols triés par aéroport de départ                          
    init_hash_table_fl_ap (ht_fl_org_ap);

    Liste_cell_hash ht_fl_dest_ap[TABLE_SIZE_AP]; // Hash table des vols triés par aéroport d'arrivée                                 
    init_hash_table_fl_ap (ht_fl_dest_ap);

    Liste_del_al ht_delay_al[TABLE_SIZE_AL]; // Hash table des airlines avec leur retard moyen associé
    init_hash_table_del_al (ht_delay_al);

    read_fl (fflights, ht_fl_al, ht_fl_date, ht_fl_org_ap, ht_fl_dest_ap, ht_delay_al, &delayed_fl);


    // TESTS REQUETES
    // char* ap = "AA";
    // delayed_airline(ht_delay_al, ht_al, ap);
    // changed_flights(ht_fl_date,1,1);
    // show_flights(ht_fl_date, "LAX",2,26,1200,5);
    // print_list_fl(delayed_fl);   //most-delayed-flights
    // show_airports(ht_fl_al, ht_ap, "HA", nb_ap);
    // print_list_fl((ht_fl_ap[443])->l);
    // avg_flight_duration(ht_fl_ap, "JFK", "LNK");
    // show_airlines(ht_fl_org_ap, ht_al, "DFW", nb_al);
    // most_delayed_airlines (ht_delay_al, ht_al, 5);
    // most_delayed_airlines_at_airports(ht_fl_dest_ap, ht_al, "LAX", 3);
    // find_itinerary(ht_fl_date,4,12,"LAX","LAS",100,5);
    // find_itinerary(ht_fl_date,6,15,"SFO","CHA",100,5);


    // LECTURE DES REQUETES DE L'UTILISATEUR
    char *liste_requetes = NULL;

    int k = 0;//indice de liste_args utilisé au moment de separer la ligne en une liste de char*
    int i = 0;//nbr de requetes effectuées

    unsigned long int taille;
    taille = LINE_SIZE;

    char *liste_args[MAX_PARAMETRES];    // liste des arguments dans une requete
    char *delim = " \0\t\n"; // delimitation du strtok (caracteres entre 2 arguments d'une requete)


    // on separe chaque ligne du fichier (1 ligne == 1 requete)
    int m = 0;
    int d =0;
    int max = 0;
    int temps = 0;

    char *str_max = NULL;
    char *month = NULL;
    char *day = NULL;

    if (getline (&liste_requetes, &taille, stdin) == -1)
    {
        printf ("Requete non lue\n");
        free (liste_requetes);
        desalloc_all_memory (ht_al, ht_ap, ht_fl_al, ht_fl_date, ht_fl_org_ap, ht_fl_dest_ap,
                             ht_delay_al, &delayed_fl);
        return 1;
    }

    do
    { // tant qu'on est pas au bout du fichier
	    if (getline (&liste_requetes, &taille, stdin) == -1)
	    {
	        printf ("Requete non lue\n");
	        free (liste_requetes);
	        desalloc_all_memory (ht_al, ht_ap, ht_fl_al, ht_fl_date, ht_fl_org_ap, ht_fl_dest_ap,
	                             ht_delay_al, &delayed_fl);
	        return 1;
	    }

        i++;
        erase_end_string (liste_requetes);
        printf("req : %s\n",liste_requetes );

        // pour toutes les requetes, on separe chaque argument
        liste_args[0] = strtok (liste_requetes, delim);
        k             = 0;
        while (liste_args[k] != NULL)
        {
            k++;
            liste_args[k] = strtok (NULL, delim);
        }

        // maintenant il faut appeler la fonction avec tous ses arguments :/
        if (strcmp (liste_args[0], "show-airports") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Liste des aéroports utilisés par la compagnie %s\n", liste_args[1]);
            show_airports (ht_fl_al, ht_ap, liste_args[1], nb_ap);
        }

        else if (strcmp (liste_args[0], "show-airlines") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Liste des compagnies partant de l'aéroport %s:\n", liste_args[1]);
            show_airlines (ht_fl_org_ap, ht_al, liste_args[1], nb_al);
        }

        else if (strcmp (liste_args[0], "show-flights") == 0)
        {
            m = atoi (strtok (liste_args[2], "-\0\n"));
            d = atoi (strtok (NULL, "\0\n"));
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Liste des vols partant de l'aéroport %s le %d/%d", liste_args[1], d, m);

            if (liste_args[3] == NULL)  // si il n'y a pas de limite ni de temps
            { 
                printf ("\n");
                show_flights (ht_fl_date, liste_args[1], m, d, INT_NULL, INT_NULL);
            }
            else if (liste_args[4] != NULL)     // si il y a une limite ET un temps
            { 
                temps = atoi (liste_args[3]);
                printf (" à partir de %dh%d\n", (int)temps / 100, temps % 100);
                show_flights (ht_fl_date, liste_args[1], m, d, temps, atoi (liste_args[4]));
            }
            else if (strstr(liste_args[3],"limit=") != NULL) // si il y a une limite en nombre (on verifie le code ASCII pour etre sur)
            { 
                printf ("\n");
                strtok (liste_args[3], "=");
                str_max = strtok (NULL, "\0\n\t");
                max     = atoi (str_max);
                show_flights (ht_fl_date, liste_args[1], m, d, INT_NULL, max);
            }
            else    // si il y a un temps mais pas de limite de nombre
            { 
                temps = atoi (liste_args[3]);
                printf (" à partir de %dh%d\n", (int)temps / 100, temps % 100);
                show_flights (ht_fl_date, liste_args[1], m, d, temps, INT_NULL);
            }
        }

        else if (strcmp (liste_args[0], "most-delayed-flights") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Liste des 5 vols les plus retardés à l'arrivée:\n");
            print_list_fl (delayed_fl);
        }

        else if (strcmp (liste_args[0], "most-delayed-airlines") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Liste des 5 compagnies qui ont le retard moyen le plus important:\n");
            most_delayed_airlines (ht_delay_al, ht_al, 5);
        }

        else if (strcmp (liste_args[0], "delayed-airline") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Affiche le retard moyen de la compagnie %s\n", liste_args[1]);
            delayed_airline (ht_delay_al, ht_al, liste_args[1]);
        }

        else if (strcmp (liste_args[0], "most-delayed-airlines-at-airport") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Affiche les 3 compagnies ayant le plus de retard à l'aéroport %s\n", liste_args[1]);
            most_delayed_airlines_at_airports (ht_fl_dest_ap, ht_al, liste_args[1], 3);
        }

        else if (strcmp (liste_args[0], "changed-flights") == 0)
        {
            month = strtok (liste_args[1], "-");
            day   = strtok (NULL, delim);
            m     = atoi (month);
            d     = atoi (day);
            printf("\n\n>>%s\n",liste_args[0]);
            printf ("     Liste des vols annulés ou déviés le %d/%d\n", d, m);
            changed_flights (ht_fl_date, m, d);
        }

        else if (strcmp (liste_args[0], "avg-flight-duration") == 0)
        {
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Affiche le temps moyen entre l'aéroport %s et %s\n", liste_args[1], liste_args[2]);
            avg_flight_duration (ht_fl_org_ap, liste_args[1], liste_args[2]);
        }

        else if (strcmp (liste_args[0], "find-itinerary") == 0)
        {
            month = strtok (liste_args[3], "-");
            day   = strtok (NULL, delim);
            m     = atoi (month);
            d     = atoi (day);
            printf("\n\n>> %s\n",liste_args[0]);
            printf ("     Affiche les itinéraires possibles pour se rendre de l'aéroport %s à %s",
                    liste_args[1], liste_args[2]);
            ;
            if (liste_args[4] == NULL)      // si il n'y a pas de limite ni de temps
            { 
            	printf("\n");
                find_itinerary (ht_fl_date, m, d, liste_args[1], liste_args[2], 0, NB_ITI_DEFAULT);
            }
            else if (liste_args[5] != NULL) // si il y a une limite ET un temps
            { 
            	printf("\n");
                temps = atoi (liste_args[4]);
                strtok (liste_args[5], "=");
                str_max = strtok (NULL, "\0\n\t");
                max     = atoi (str_max);

                printf (" à partir de %dh%d\n", (int) temps / 100, temps % 100);
                find_itinerary (ht_fl_date, m, d, liste_args[1], liste_args[2], temps, max);
            }
            else if (liste_args[4][0] == 'l')   // si il y a une limite en nombre (on verifie le code ASCII pour etre sur)
            { 
            	printf("\n");
                strtok (liste_args[4], "=");
                str_max = strtok (NULL, "\0\n\t");
                max     = atoi (str_max);
                find_itinerary (ht_fl_date, m, d, liste_args[1], liste_args[2], 0, max);
            }
            else            // si il y a un temps mais pas de limite de nombre
            { 
                temps = atoi (liste_args[4]);
                printf (" à partir de %dh%d\n", (int)temps / 100, temps % 100);
                find_itinerary (ht_fl_date, m, d, liste_args[1], liste_args[2], temps, NB_ITI_DEFAULT);
            }
        }

        else if(strcmp(liste_args[0],"find-multicity-itinerary")==0){ // requete non implémentée
        	find_multicity_itinerary(ht_fl_date,liste_args);
        }

        else if (strncmp (liste_args[0], "quit", 4) == 0)
            break;

        else
            printf ("Requete non reconnue.\n");

    } while (*liste_requetes != '\0' && i < MAX_REQUETES);


    // LIBÉRATION DE LA MÉMOIRE
    free (liste_requetes);
    desalloc_all_memory (ht_al, ht_ap, ht_fl_al, ht_fl_date, ht_fl_org_ap, ht_fl_dest_ap, ht_delay_al, &delayed_fl);

    // FERMETURE DES FICHIERS
    fclose (fairlines);
    fclose (fairports);
    fclose (fflights);
    printf ("Fichiers fermés\n");

    return 0;
}