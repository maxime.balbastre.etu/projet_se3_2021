#include "../headers/requetes.h"

// STRTOK MODIFIE cette version renvoi NULL si le champ est vide (2 virgules d'affilées par exemple)
char *strtok_r_m (char *s, const char *delim, char **save_ptr)
{
    char *end;
    if (s == NULL) s = *save_ptr;
    if (*s == '\0')
    {
        *save_ptr = s;
        return NULL;
    }
    /* Scan leading delimiters.  */
    if (strspn (s, delim) != 0)
    {
        *save_ptr = s + 1;
        return NULL;
    }

    /* Find the end of the token.  */
    end = s + strcspn (s, delim);
    if (*end == '\0')
    {
        *save_ptr = end;
        return s;
    }
    /* Terminate the token and make *SAVE_PTR point past it.  */
    *end      = '\0';
    *save_ptr = end + 1;
    return s;
}

char *strtok_m (char *s, const char *delim)
{
    static char *olds;
    return strtok_r_m (s, delim, &olds);
}

void erase_end_string (char *line)      // Supprime le dernier élement d'une string
{
    int c = 0;
    while (line[c + 1] != '\0')
    {
        c++;
    }
    line[c] = '\0';
}


// FONCTIONS DE LECTURES DES FICHIERS CSV
int read_al (FILE *fairlines, Liste_al table[])
{

    int nb_al = 0;

    char *      line = NULL;
    size_t      len  = 0;
    const char *s    = ",;\0\n";

    if (getline (&line, &len, fairlines) == -1)
    {
        exit (EXIT_FAILURE);
    }

    while (getline (&line, &len, fairlines) != -1)
    {
        erase_end_string (line);

        Al *new = malloc (sizeof (Al));

        char *token = strtok_m (line, s);
        strcpy (new->IATA_line, token);

        token = strtok_m (NULL, s);
        strcpy (new->name, token);

        ajout_hash_table_al (table, &new);
        nb_al++;
    }
    free (line);
    return nb_al;
}

int read_ap (FILE *fairports, Liste_ap table[])
{

    int nb_ap = 0;

    char *      line = NULL;
    size_t      len  = 0;
    const char *s    = ",;\0\n";

    if (getline (&line, &len, fairports) == -1)
    {
        exit (EXIT_FAILURE);
    }

    while (getline (&line, &len, fairports) != -1)
    {
        erase_end_string (line);
        Ap *new = malloc (sizeof (Ap));

        char *token = strtok_m (line, s);
        strcpy (new->IATA_port, token);

        token = strtok_m (NULL, s);
        strcpy (new->name, token);

        token = strtok_m (NULL, s);
        strcpy (new->ville, token);

        token = strtok_m (NULL, s);
        strcpy (new->state, token);

        ajout_hash_table_ap (table, &new);
        nb_ap++;
    }
    free (line);
    return nb_ap;
}


int read_fl (FILE *          fflights,
             Liste_fl        tab_line[],
             Liste_fl        tab_date[],
             Liste_cell_hash tab_org_ap[],
             Liste_cell_hash tab_dest_ap[],
             Liste_del_al    tab_del_al[],
             Liste_fl *      delayed_fl)
{

    int         nb_fl = 0;
    char *      line  = NULL;
    size_t      len   = 0;
    const char *s     = ",;\0\t"; // séparateurs pour le strtok

    if (getline (&line, &len, fflights) == -1)
    {
        exit (EXIT_FAILURE);
    }

    while (getline (&line, &len, fflights) != -1)
    {
        erase_end_string (line);

        Fl *new = malloc (sizeof (Fl));

        char *token = strtok_m (line, s);
        new->month  = atoi (token);

        token    = strtok_m (NULL, s);
        new->day = atoi (token);

        token        = strtok_m (NULL, s);
        new->weekday = atoi (token);

        token = strtok_m (NULL, s);
        strcpy (new->airline, token);

        token = strtok_m (NULL, s);
        strcpy (new->org_ap, token);

        token = strtok_m (NULL, s);
        strcpy (new->dest_ap, token);

        token = strtok_m (NULL, s);
        if (token == NULL)
            new->sched_dep = INT_NULL;
        else
            new->sched_dep = atoi (token);

        token = strtok_m (NULL, s);
        if (token == NULL)
            new->dep_delay = (float)INT_NULL;
        else
        {
            new->dep_delay = atof (token);
        }

        token = strtok_m (NULL, s);
        if (token == NULL)
            new->air_time = (float)INT_NULL;
        else
            new->air_time = atof (token);

        token = strtok_m (NULL, s);
        if (token == NULL)
            new->dist = INT_NULL;
        else
            new->dist = atoi (token);

        token = strtok_m (NULL, s);
        if (token == NULL)
            new->sched_arr = INT_NULL;
        else
            new->sched_arr = atoi (token);

        token = strtok_m (NULL, s);
        if (token == NULL)
            new->arr_delay = (float)INT_NULL;
        else
            new->arr_delay = atof (token);

        token         = strtok_m (NULL, s);
        new->diverted = atoi (token);

        token          = strtok_m (NULL, s);
        new->cancelled = atoi (token);

        nb_fl++;
        
        // Insertion du vol dans chaque SD
        ajout_hash_table_fl_date (tab_date, &new);
        ajout_hash_table_fl_ap (tab_org_ap, &new);
        ajout_hash_table_fl_ap (tab_dest_ap, &new);
        ajout_hash_table_fl (tab_line, &new, hash_function, new->airline);
        incr_hash_table_delay_al (tab_del_al, new->airline, new->arr_delay);
        ajout_most_delayed_flights (delayed_fl, &new, nb_fl);
    }
    free (line);
    return nb_fl;
}


void desalloc_all_memory (Liste_al        tab_al[],
                          Liste_ap        tab_ap[],
                          Liste_fl        tab_line[],
                          Liste_fl        tab_date[],
                          Liste_cell_hash tab_org_ap[],
                          Liste_cell_hash tab_dest_ap[],
                          Liste_del_al    tab_del_al[],
                          Liste_fl *      delayed_fl)
{
    desalloc_table_hash_al (tab_al, TABLE_SIZE_AL);
    desalloc_table_hash_ap (tab_ap, TABLE_SIZE_AP);

    desalloc_struct_fl (tab_line);
    desalloc_table_hash_fl (tab_line, TABLE_SIZE_AL);
    desalloc_table_hash_fl (tab_date, TABLE_SIZE_DATE);
    desalloc_table_hash_fl_ap (tab_org_ap, TABLE_SIZE_AP);
    desalloc_table_hash_fl_ap (tab_dest_ap, TABLE_SIZE_AP);

    desalloc_table_hash_del_al (tab_del_al, TABLE_SIZE_AL);
    desalloc_liste_fl (delayed_fl);
}