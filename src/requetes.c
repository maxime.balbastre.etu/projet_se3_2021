#include "../headers/hash_table.h"
#include <stdarg.h>


// DELAYED AIRLINE
void delayed_airline (Liste_del_al t[], Liste_al t_al[], char asked_al[])
{

    int ind = hash_function (asked_al);
    if (t[ind] == NULL)
    {
        printf ("La compagnie de IATA code %s n'a aucun vol enregistré.\n", asked_al);
        return;
    }

    Cell_del_al *p = t[ind];
    while (p != NULL && strcmp (p->IATA, asked_al) != 0) p = p->suiv;
    if (p == NULL)
    {
        printf ("La compagnie de IATA code %s n'a aucun vol enregistré.\n", asked_al);
        return;
    }

    // Récupération du nom de la airline dans la table de hachage des airlines
    Cell_al *p2 = t_al[ind];
    while (strcmp (asked_al, p2->a->IATA_line) != 0) p2 = p2->suiv;

    // Affichage final
    printf ("%s, %s, average delay : %.2f minutes\n", asked_al, p2->a->name,
            p->avg_delay / p->nb_fl);
}

// MOST-DELAYED-ARILINES (5 FONCTIONS)
void ajout_tete_delay_al (Liste_del_al *pl, Cell_del_al *a)
{
    Cell_del_al *c = malloc (sizeof (Cell_del_al));
    strcpy (c->IATA, a->IATA);
    c->avg_delay = a->avg_delay;
    c->nb_fl     = a->nb_fl;
    c->suiv      = *pl;
    *pl          = c;
}

void supp_queue_delay_al (Liste_del_al *pl)
{
    if (*pl == NULL) return;
    Cell_del_al *p = *pl;
    while (p->suiv->suiv != NULL) p = p->suiv;
    Cell_del_al *temp = p->suiv;
    p->suiv           = NULL;
    free (temp);
}

void ajout_dec_delay_al (Liste_del_al *pl, Cell_del_al *pc)
{
    if (*pl == NULL)
    {
        ajout_tete_delay_al (pl, pc);
        return;
    }
    if ((pc->avg_delay / pc->nb_fl) > ((*pl)->avg_delay / (*pl)->nb_fl))
    {
        ajout_tete_delay_al (pl, pc);
        return;
    }
    Cell_del_al *p = *pl;
    while (p->suiv != NULL &&
           (pc->avg_delay / pc->nb_fl) < p->suiv->avg_delay / p->suiv->nb_fl)
        p = p->suiv;
    ajout_tete_delay_al (&p->suiv, pc);
}

void print_liste_delay_al (Cell_del_al *l, Liste_al t_al[])
{
    Cell_del_al *pc = l;
    int          ind;
    while (pc != NULL)
    {
        ind         = hash_function (pc->IATA);
        Cell_al *pa = t_al[ind];
        while (pa != NULL && strcmp (pa->a->IATA_line, pc->IATA) != 0) // gestion collision
            pa = pa->suiv;
        printf ("%s %s : %.2f min en moyenne.\n", pa->a->IATA_line, pa->a->name,
                pc->avg_delay / pc->nb_fl);
        pc = pc->suiv;
    }
}

void most_delayed_airlines (Liste_del_al t[], Liste_al t_al[], int max)
{

    // Construction de la listes comprenant les 5 compagnies aériennes avec le plus de retard en moyenne
    Liste_del_al l = NULL;
    int          n = 0;
    for (int i = 0; i < TABLE_SIZE_AL; ++i)
    { // on parcout toute la table
        if (t[i] != NULL)
        {
            Cell_del_al *p = t[i];
            while (p != NULL)
            { // gestion collisions
                ajout_dec_delay_al (&l, p);
                n++;
                if (n > max)
                    supp_queue_delay_al (&l); // on supprime la dernière cellule pour ne garder que 5 comapgnies
                p = p->suiv;
            }
        }
    }
    // Affichage de la liste : récupération des infos dans la table des Airlines
    print_liste_delay_al (l, t_al);

    // Libération mémoire de la liste temporaire
    desalloc_liste_del_ap (&l);
}

// MOST DELAYED AIRLINES AT AIRPORT
void most_delayed_airlines_at_airports (Liste_cell_hash t_ap[], Liste_al t_al[], char *ap, int max)
{
    int          ind = hash_func_IATA_p (ap);
    Liste_del_al tmp[TABLE_SIZE_AL]; // Table de hashage temporaire contenant le délai moyen de chaque airlines arrivant à l'aéroport ap demandé
    init_hash_table_del_al (tmp);

    // Recherche de la cellule correspondante (collisions)
    Cell_hash *pc = t_ap[ind];
    while (pc != NULL && strcmp (pc->key, ap) != 0) pc = pc->suiv;
    if (pc == NULL)
    {
        printf ("Aucun vols arrivant à l'aéroport %s.\n", ap);
        return;
    }

    // Parcours de la liste de tous les vols arrivant à l'aéroport ap
    Cell_fl *p = pc->l;
    while (p != NULL)
    {
        incr_hash_table_delay_al (tmp, p->fl->airline, p->fl->arr_delay);
        p = p->suiv;
    }
    most_delayed_airlines (tmp, t_al, max);
    desalloc_table_hash_del_al (tmp, TABLE_SIZE_AL);
}

// CHANGED FLIGHT
void changed_flights (Liste_fl tab[], int m, int d)
{

    int      ind           = hash_func_date (m, d);
    Cell_fl *p             = tab[ind];
    int      nb_changed_fl = 0;
    while (p != NULL)
    {
        // printf("%d\n",ind);
        if ((p->fl->cancelled == 1) || (p->fl->diverted == 1))
        {
            print_fl (p->fl);
            nb_changed_fl++;
        }
        p = p->suiv;
    }
    if (nb_changed_fl == 0) printf ("Aucun vol annulé ou dévié ce jour\n");
}

// SHOW FLIGHTS
void show_flights (Liste_fl t[], char *IATA_ap, int m, int d, int time, int max)
{

    int      ind = hash_func_date (m, d);
    Cell_fl *p   = t[ind];
    int      nb  = 0;
    while (p != NULL)
    {
        if (strcmp (p->fl->org_ap, IATA_ap) == 0)
        { // test l'aeroport de départ
            if ((unsigned)time == INT_NULL && (unsigned)max == INT_NULL)  // aucune option
            { 
                print_fl (p->fl);
                nb++;
            }
            else if ((unsigned)time == INT_NULL && nb < max)   // uniquement l'option limit=xx
            { 
                print_fl (p->fl);
                nb++;
            }
            else if ((unsigned)max == INT_NULL && (p->fl->sched_dep) > time)  // uniquement l'option time
            { 
                print_fl (p->fl);
                nb++;
            }
            else if (nb < max && (p->fl->sched_dep) > time)   // les deux options
            { 
                print_fl (p->fl);
                nb++;
            }
        }
        p = p->suiv;
    }
    if (nb == 0)
        printf ("Aucun vol ne part de l'aéroport %s ce jour.\n", IATA_ap);
}


// MOST DELAYED FLIGHTS
void ajout_most_delayed_flights (Liste_fl *pl, Fl **new, int nb_fl)
{
    if ((*new)->arr_delay == (float)INT_NULL) // si le vol n'a pas de valeurs pour le champ arr_delay on ne l'ajoute pas
        return;
    if (nb_fl <= 5)
    {
        ajout_dec_fl (pl, new);
        return;
    }
    ajout_dec_fl (pl, new);
    supp_queue_fl (pl);
}

// SHOW AIRPORTS (5 fonctions)
void print_list_s (Liste_s l)
{
    if (l == NULL) printf ("vide\n");
    while (l != NULL)
    {
        printf ("%s->", l->s);
        l = l->suiv;
    }
    printf ("\n");
}

int ajout_ordo_s (Liste_s *pl, char s[], int nb)    // ajout ordonné d'une string dans une liste chainée 
{                                                   // (ordonnée par la première lettre de la chaine)
    if (*pl == NULL)
    {
        ajout_tete_s (pl, s);
        return nb + 1;
    }
    Cell_s *p = *pl;
    while (p->suiv != NULL)
    {
        if (strcmp (s, p->s) == 0) return nb;
        p = p->suiv;
    }
    if (strcmp (s, p->s) == 0) return nb;
    ajout_tete_s (pl, s);
    return nb + 1;
}

void show_airports (Liste_fl t_fl[], Liste_ap t_ap[], char *al, int nb_ap)
{
    int ind      = hash_function (al);
    int nb_ap_aj = 0;           // nombre d'aéroport ajouté
    Liste_s  l   = NULL;        // liste qui va contenir tous les <port-id> par lesquels la compagnie al passe
    Cell_fl *p   = t_fl[ind];

    if (p == NULL)
    {
        printf ("La compagnie %s n'a aucun vol enregistré.\n", al);
        return;
    }

    // Remplissage de la liste
    while (p != NULL && nb_ap_aj < nb_ap)
    {
        if (strcmp (p->fl->airline, al) == 0)
        { // test collision
            nb_ap_aj = ajout_ordo_s (&l, p->fl->org_ap, nb_ap_aj);
            nb_ap_aj = ajout_ordo_s (&l, p->fl->dest_ap, nb_ap_aj);
        }
        p = p->suiv;
    }
    if (nb_ap_aj == 0)
    {
        printf ("La compagnie %s n'a aucun vol enregistré.\n", al);
        return;
    }

    // Affichage de la liste (on va chercher les infos correspondantes)
    Cell_s *ps = l;
    while (ps != NULL)
    {
        ind         = hash_func_IATA_p (ps->s);
        Cell_ap *pa = t_ap[ind];
        while (pa != NULL && strcmp (pa->a->IATA_port, ps->s) != 0) // gestion collision
            pa = pa->suiv;
        print_ap (pa->a);
        ps = ps->suiv;
    }

    // Libération de la mémoire
    desalloc_liste_s (&l);
}

// SHOW AIRLINES (utilise des fonctions définies juste au dessus pour la requete show-aiports)
void show_airlines (Liste_cell_hash t[], Liste_al t_al[], char *ap, int nb_al)
{
    int ind      = hash_func_IATA_p (ap);
    int nb_al_aj = 0;       // nombre d'airlines ajoutées
    Liste_s l    = NULL;    // liste qui va contenir tous les <airline-id> qui ont des vols au départ de ap

    // Recherche de la cellule correspondant à l'aéroport choisi par l'utilisateur
    Cell_hash *pc = t[ind];
    while (pc != NULL && strcmp (pc->key, ap) != 0) pc = pc->suiv;
    if (pc == NULL) printf ("Aucun vols au départ de l'aéroport %s.\n", ap);

    // Remplissage de la liste
    Cell_fl *p = pc->l;
    while (p != NULL && nb_al_aj < nb_al)
    {
        nb_al_aj = ajout_ordo_s (&l, p->fl->airline, nb_al_aj);
        nb_al_aj = ajout_ordo_s (&l, p->fl->airline, nb_al_aj);
        p        = p->suiv;
    }

    // Affichage de la liste (on va chercher les infos correspondantes)
    Cell_s *ps = l;
    while (ps != NULL)
    {
        ind         = hash_function (ps->s);
        Cell_al *pa = t_al[ind];
        while (pa != NULL && strcmp (pa->a->IATA_line, ps->s) != 0) // gestion collision
            pa = pa->suiv;
        print_al (pa->a);
        ps = ps->suiv;
    }

    // Libération de la mémoire
    desalloc_liste_s (&l);
}

// AVERAGE FLIGHT DURATION
void avg_flight_duration (Liste_cell_hash t[], char *dep, char *arr)
{
    int ind = hash_func_IATA_p (dep);

    // Recherche de la cellule correspondant à l'aéroport de départ choisi par l'utilisateur
    Cell_hash *pc = t[ind];
    while (pc != NULL && strcmp (pc->key, dep) != 0) pc = pc->suiv;
    if (pc == NULL) printf ("Aucun vols au départ de l'aéroport %s.\n", dep);

    // Calcul de la durée
    float s = 0; // somme des durées
    int nb = 0;  // nombre de vols reliant dep à arr
    Cell_fl *p = pc->l;
    while (p != NULL)
    {
        if (strcmp (p->fl->dest_ap, arr) == 0 && p->fl->air_time != (float)INT_NULL)
        {
            s += p->fl->air_time;
            nb++;
        }
        p = p->suiv;
    }
    // Affichage final
    if (nb == 0)
        printf ("Aucun vols à destination de %s ne part de l'aéroport %s.\n", arr, dep);

    printf ("Durée moyenne: %f minutes (%d vols).\n", s / nb, nb);
}


// FIND ITINERARY

void print_list_fl_inverse (Liste_fl l)
{
    if (l != NULL)
    {
        print_list_fl_inverse (l->suiv);
        print_fl (l->fl);
    }
}

void find_flight (Liste_fl p, Liste_fl *iti, char *dep, char *stop, char *dest, int time, int max_iti, int nb_escales, int *pt_nb_iti)
{
    Cell_fl *l = p;
    while (l != NULL)
    {
        if (*pt_nb_iti >= max_iti) 
            return;

        if (strcmp (l->fl->org_ap, stop) == 0 && l->fl->sched_dep > time && l->fl->cancelled == 0 &&
            l->fl->diverted == 0)
        {
            if (strcmp (l->fl->dest_ap, dest) == 0) // si l'aéroport d'arrivée est le bon, on print la liste
            {
                ajout_tete_fl (iti, &(l->fl));
                print_list_fl_inverse (*iti);
                printf ("\n");
                (*pt_nb_iti)++;
                l = l->suiv;
                supp_tete_fl (iti); // on supprime le dernier element ajouté
                continue; // on continue pour chercher un itinéraire avec le début de trajet enregistré
            }

            if (nb_escales + 1 > MAX_ESCALES) 
                break;

            ajout_tete_fl (iti, &(l->fl));

            find_flight (p, iti, dep, l->fl->dest_ap, dest, l->fl->sched_dep + MIN_TIME_ESCALE,
                         max_iti, nb_escales + 1, pt_nb_iti);
        }
        l = l->suiv;
    }
    supp_tete_fl (iti);
    return;
}

int find_itinerary (Liste_fl t_date[], int m, int d, char *dep, char *dest, int time, int max_iti)
{

    int      ind           = hash_func_date (m, d);
    Liste_fl fl_of_the_day = t_date[ind];
    Liste_fl iti           = NULL; // liste recevant l'itinéraire
    int      nb_iti        = 0;    // nombre d'itinéraires affichés

    find_flight (fl_of_the_day, &iti, dep, dep, dest, time, max_iti, 0, &nb_iti);

    if (nb_iti == 0)
    {
    	printf ("Aucun itinéraire trouvé.\n");
        desalloc_liste_fl(&iti);
    	return 0;
    }
    desalloc_liste_fl(&iti);
    return 1;
}

void find_multicity_itinerary(Liste_fl t_date[], char *liste_args[])
{
	char *month,*day, *dep, *dest, *date;
	int m,d,time, vol_existe, i=2;
	//va_list arg_ptr;
	//va_start(arg_ptr,dep);
	//char *test_arg = va_arg(arg_ptr,char*);
	dep = liste_args[1];

	while(liste_args[i] != NULL)//tant qu'il reste des arguments à lire (le dernier vaut NULL)
	{
		dest = liste_args[i];//lecture de la destination

		i++;
		date = liste_args[i];//lecture de la date
		month = strtok(date,"-");
		day = strtok(NULL," \0\n\t");
		m = atoi(month);
		d = atoi(day);
		
		i++;//liste_args[i] est maintenant soit une heure soit un IATA_ap
		//printf("%d ",liste_args[i][0]);
		printf("De %s à %s\n",dep,dest);
		if(liste_args[i] != NULL && liste_args[i][0] >= 48 && liste_args[i][0] <= 57){//si une heure est donnée (30 == "0" et 39 == "9")
			time = atoi(liste_args[i]);
			
			vol_existe = find_itinerary(t_date,m,d,dep,dest,time,1);

			i++;//liste_args[i] devient maintenant un IATA_ap (sauf si on est au dernier vol)
			dep = dest;
		}
		else
		{
			vol_existe = find_itinerary(t_date,m,d,dep,dest,0,1);
			dep = dest;
		}

		if(!vol_existe) return;
	}
}