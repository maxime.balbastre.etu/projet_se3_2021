# Projet de Programmation Avancée de Maxime BALBASTRE et Yanis LACROIX(SE3 - 2020/2021)


## Description du projet

Ce projet a permis d’implémenter une application qui permet d'analyser 58592 vols ayant eu lieu aux États Unis en 2014. Ce programme permet en effet à l’utilisateur d’exécuter des requêtes sur la base de données fournies.

## Contenu du dépôt

Le dépôt est organisé en plusieurs dossiers :

- `data`  : 
  	* airports.csv 
    * airlines.csv
    * flights.csv


Ces trois fichiers sont au format csv qui est un format texte permettant de stocker des tableaux. Chaque ligne du fichier correspond à une ligne du tableau et les différents éléments d'une ligne sont
séparés par un élément particulier, ici ‘ , ’. 
Attention, dans le fichier flights, certains champs peuvent être vides (‘ ,, ’).

   	* requetes.txt qui est un exemple de fichier de requêtes que l’exécutable peut lire.


- `headers`  : 
	* _structures.h_
	* _linked_list.h_
	* _hash_table.h_
	* _requetes.h_
	* _read_csv.h_

le volume de données étant relativement important, un soin particulier a été apporté aux structures de données utilisées dans les fichiers sources suivants et à la rapidité d'exécution du programme. Elles sont définies dans le header éponyme. Les choix algorithmiques sont expliqués et justifiés dans `rapport.pdf`.

- `src` :
	* *structures.c*
	* *linked_list.c*
	* *hash_table.c*
	* *requetes.c*
	* *read_csv.c*
	* *main.c*


- `Makefile`  :

Le projet peut être compilé à l'aide de la commande `make`, qui permet de compiler l’ensemble des fichiers sources en créant un exécutable **projet** à la racine du dépôt. Tous les fichiers objets issus de la compilation sont enregistrés dans **/obj**. La fonction make clean permet de supprimer tous les fichiers objets, supprimer l’exécutable et retirer le dossier **obj/**.

- `.clang-format` : 

C'est le fichier de formatage clang personnel qui a été utilisé pour formater tous les fichiers du dépôt.

## Utilisation du programme

Ce programme, comme expliqué précedemment, est une application qui fait l'interface entre l'utilisateur et la base de données. Pour soumettre une ou des requêtes, 2 optioins sont possibles :
	- écrire les requêtes à l'aide du terminal
	- écrire les requêtes dans un fichier .txt et le rediriger sur l'entrée standard à l'exécution du prgramme, à l'aide d'une commande qui pourrait être similaire à :

				`$ ./projet < data/requetes.txt`


A noter qu'un fichier **requetes.txt** est disponible dans le dossier data. Il peut être modifié. Néanmoins, dans tous les cas, les requêtes doivent respecter une synthaxe très spécifique. Ci-dessous l'ensemble des requêtes implémentées et leur synthaxe associé.


- `show-airports <airline_id>`  : affiche tous les aéroports depuis lesquels la compagnie aérienne `<airline_id>` opère des vols
- `show-airlines <port_id>`: affiche les compagnies aériens qui ont des vols qui partent de l'aéroport `<port_id>`
- `show-flights <port_id> <date> [<time>] [limit=<xx>]` : affiche les vols qui partent de l'aéroport à la date, avec optionnellement une heure de début, et limité à xx vols
- `most-delayed-flights`     : donne les 5 vols qui ont subis les plus longs retards à l'arrivée
- `most-delayed-airlines`    : donne les 5 compagnies aériennes qui ont, en moyenne, le plus de retards
- `delayed-airline <airline_id>`    : donne le retard moyen de la compagnie aérienne passée en paramètre
- `most-delayed-airlines-at-airport <airport_id>`    : donne les 3 compagnies aériennes avec le plus de retard d'arrivé à l'aéroport passée en paramètre
- `changed-flights <date>` : les vols annulés ou déviés à la date <date> 
- `avg-flight-duration <port_id1> <port_id2>`: calcule le temps de vol moyen entre deux aéroports dans le sens port1 -> port2
- `find-itinerary <port_id> <port_id> <date> [<time>] [limit=<xx>]`: trouve un ou plusieurs itinéraires entre deux aéroports à une date donnée (l'heure est optionnel, requête peut être limité à `xx` propositions, il peut y avoir des escales)
- `find-multicity-itinerary <port_id_depart> <port_id_dest1> <date> [<time>] <port_id_dest2> <date> [<time>] ... <port_id_destN> <date> [<time>]`: trouve un itinéraire multiville qui permet de visiter plusieurs villes (il peut y avoir des escales pour chaque vol intermediaire)
- `quit`       : quit


Pour information, les paramètres entre crochets `[ ]` sont optionnels et les paramètres entre `< >` indiquent une valeur à renseigner. Les dates sont au format `M-D` et l'heure `HHMM`.

Certaines valeurs par défaut sont à connaître :

- Le maximum d'escale pour un itinéraire est fixé à 3
- Le minimum de temps pour une escale est fixé à 30 minutes
- Si l'utilisateur ne spécifie pas de valeurs pour le nombre d'itinéraires affichés, il sera fixé à 3
- L'utilisateur ne peut pas demander plus de 100 requêtes, il devra relancer le programme s'il souhaite en faire plus


## Affichage proposé

### `show-airports <airline_id>`

> Exemple et affichage

~~~
>> show-airports
     Liste des aéroports utilisés par la compagnie HA:
KOA Kona International Airport at Keahole Kailua/Kona HI
LAS McCarran International Airport Las Vegas NV
SFO San Francisco International Airport San Francisco CA
OGG Kahului Airport Kahului HI
PHX Phoenix Sky Harbor International Airport Phoenix AZ
LIH Lihue Airport Lihue HI
HNL Honolulu International Airport Honolulu HI
LAX Los Angeles International Airport Los Angeles CA

~~~

### `show-airlines <port_id>`

> Exemple et affichage

~~~
>> show-airlines
     Liste des compagnies partant de l'aéroport LAX:
US US Airways Inc.
MQ American Eagle Airlines Inc.
F9 Frontier Airlines Inc.
NK Spirit Air Lines
UA United Air Lines Inc.
OO Skywest Airlines Inc.
VX Virgin America
B6 JetBlue Airways
HA Hawaiian Airlines Inc.
AS Alaska Airlines Inc.
AA American Airlines Inc.
WN Southwest Airlines Co.
DL Delta Air Lines Inc.


~~~


### `show-flights <port_id> <date> [<time>] [limit=<xx>]`

> Exemple et affichage

~~~
>> show-flights
     Liste des vols partant de l'aéroport LAX le 7/3
3,7,6,OO,LAX,JAC,815,-10.00,130.00,784,1140,0.00,0,0
3,7,6,UA,LAX,OGG,1437,95.00,354.00,2486,1832,142.00,0,0
3,7,6,AS,LAX,SEA,1940,-1.00,134.00,954,2220,-2.00,0,0
3,7,6,AS,LAX,PDX,1740,-6.00,117.00,834,2000,-2.00,0,0
3,7,6,UA,LAX,SFO,1955,-6.00,52.00,337,2126,-4.00,0,0
3,7,6,AA,LAX,STL,1528,-4.00,188.00,1592,2059,-12.00,0,0
3,7,6,OO,LAX,MRY,2009,-8.00,44.00,266,2118,-16.00,0,0
3,7,6,AA,LAX,BNA,2225,33.00,203.00,1797,514,25.00,0,0
3,7,6,B6,LAX,JFK,2111,-4.00,281.00,2475,622,0.00,0,0
3,7,6,NK,LAX,MSP,922,-3.00,193.00,1535,1455,-7.00,0,0
3,7,6,AA,LAX,DFW,1700,1.00,149.00,1235,2156,-2.00,0,0
~~~

### `most-delayed-flights`
> Exemple et affichage

~~~
>> most-delayed-flights
     Liste des 5 vols les plus retardés à l'arrivée:
12,30,3,UA,DEN,LAX,1120,1194.00,121.00,862,1300,1185.00,0,0
7,4,6,AA,IAH,DFW,1749,835.00,49.00,224,1906,858.00,0,0
2,4,3,F9,DFW,DEN,1000,852.00,97.00,641,1104,839.00,0,0
8,1,6,DL,DFW,ATL,815,755.00,94.00,731,1124,741.00,0,0
1,2,5,AA,LAS,JFK,810,751.00,254.00,2248,1605,732.00,0,0


~~~

### `most-delayed-airlines`

> Exemple et affichage

~~~
>> most-delayed-airlines
     Liste des 5 compagnies qui ont le retard moyen le plus important:
NK Spirit Air Lines : 43.38 min en moyenne.
F9 Frontier Airlines Inc. : 40.88 min en moyenne.
UA United Air Lines Inc. : 39.56 min en moyenne.
B6 JetBlue Airways : 36.75 min en moyenne.
AA American Airlines Inc. : 36.59 min en moyenne.
~~~

### `delayed-airline <airline_id>`

> Exemple et affichage

~~~
>> delayed-airline
     Affiche le retard moyen de la compagnie AA
AA, American Airlines Inc., average delay : 36.59 minutes
~~~


### `most-delayed-airlines-by-airports <airport_id>`

> Exemple et affichage

~~~
>> most-delayed-airlines-at-airport
     Affiche les 3 compagnies ayant le plus de retard à l'aéroport LAX
NK Spirit Air Lines : 50.18 min en moyenne.
UA United Air Lines Inc. : 37.92 min en moyenne.
MQ American Eagle Airlines Inc. : 33.60 min en moyenne.
~~~


### `changed-flights <date>`

> Exemple et affichage

~~~
>>changed-flights
     Liste des vols annulés ou déviés le 1/1
1,1,4,MQ,DFW,PNS,1035,,,604,1213,,0,1
1,1,4,MQ,DFW,LRD,1255,,,396,1418,,0,1
1,1,4,EV,DFW,LAW,1130,,,140,1215,,0,1
1,1,4,MQ,DFW,SJT,2155,,,229,2253,,0,1
1,1,4,AA,DFW,ICT,1000,,,328,1115,,0,1
1,1,4,OO,PHX,SLC,950,,,507,1138,,0,1
1,1,4,MQ,DFW,BTR,730,,,383,853,,0,1
~~~

### `avg-flight-duration <port_id> <port_id>`

> Exemple et affichage

~~~
>> avg-flight-duration
     Affiche le temps moyen entre l'aéroport LAX et JFK
Durée moyenne: 292.016174 minutes (371 vols).
~~~

### `find-itinerary <port_id> <port_id> <date> [<time>] [limit=<xx>]`

> Exemple et affichage

~~~
>> find-itinerary
     Affiche les itinéraires possibles pour se rendre de l'aéroport LAX à SFO
3,7,6,UA,LAX,SFO,1955,-6.00,52.00,337,2126,-4.00,0,0

3,7,6,NK,LAX,MSP,922,-3.00,193.00,1535,1455,-7.00,0,0
3,7,6,DL,MSP,SFO,1230,-2.00,205.00,1589,1434,-23.00,0,0

3,7,6,AA,LAX,DFW,1700,1.00,149.00,1235,2156,-2.00,0,0
3,7,6,AA,DFW,SFO,1740,10.00,194.00,1464,1933,-10.00,0,0
~~~


### `find-multicity-itinerary <port_id_depart> <port_id_dest1> <date> [<time>] <port_id_dest2> <date> [<time>] ... <port_id_destN> <date> [<time>]`

> Exemple et affichage

~~~
>> find-multicity-itinerary
   Affiche un itinéraire possible pour se rendre d'un aéroport à l'autre

De ATL à LAX
2,26,4,NK,ATL,IAH,1545,58.00,125.00,689,1708,61.00,0,0
2,26,4,UA,IAH,LAX,1706,2.00,200.00,1379,1853,-2.00,0,0

De LAX à JAC
3,7,6,OO,LAX,JAC,815,-10.00,130.00,784,1140,0.00,0,0
~~~





	

